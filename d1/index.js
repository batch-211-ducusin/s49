console.log("Hello");


let posts = [];
let count = 1;
/*
	today we are going to simulate an interactive web page using DOM and fetching data from a server.

fetch Method
	syntax:
		fetch("url", options)
	Accepts two arguments:
		1 URL - this is the url which the request is to be made.
		2 options - used only when we need the details of the request from the user.

	fetch() method in JS is used to send requests in the server and load the received responce in the webpage. The request and responce is in JSON format.
*/

// Get Post Data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response)=> response.json()) //ginawa ko na JSON format ang response.
.then((data)=> showPosts(data)); //kinomand ko na iprint ang result.
/*
	we use fetch method to get the posts inside https://jsonplaceholder.typicode.com/posts
	make the response in json format (.then)
*/


// Add Post Data
// ITO and mag pupush ng entries natin papunta sa empty array natin na "let posts = [];"
document.querySelector("#form-add-post").addEventListener("submit", (e)=> {

	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {"Content-type": "application/json; charset=UTF-8"}
	})
	.then((response)=> response.json()) //converts the response into JSON format
	.then((data)=> {
		console.log(data);
		alert("Successfully added.");
		document.querySelector("#txt-title").value = null; //ito ang mag cclear ng inputs after 
		document.querySelector("#txt-body").value = null;
	})
});

/*
	mini activity
	Create a showPost function
*/



//Add Post Data
// ITO and mag pupush ng entries natin papunta sa empty array natin na "let posts = [];"
/*document.querySelector("#form-add-post").addEventListener("submit", (e)=> {
	e.preventDefault() //prevent the page from loading

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	count++
	console.log(posts);
	alert("successfully added!");
	//invoke a function to show post later.
	showPosts()
});*/


//Show Posts
// ITO ang mag lalagay ng laman sa loob ng <div> na may id="div-post-entries"
const showPosts = (posts)=> {
	let postEntry = "";
	posts.forEach((post)=> {
		postEntry += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntry);
	document.querySelector("#div-post-entries").innerHTML = postEntry;
};


// Edit Post
const editPost = (id)=> {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	//Pass the id, title, and body from the post to be updated in the edit post form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;


	//Add Attribute
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
};


//Update Post
document.querySelector("#form-add-post").addEventListener("submit", (e)=> {
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {"Content-type": "application/json; charset=UTF-8"}
	})
	.then((response)=> response.json())
	.then((data)=> {
		console.log(data);
		alert("Successfully updated.");
		document.querySelector("#txt-edit-id").value = null;
		//ito ang mag cclear ng inputs sa id after pindutin ang create button.
		document.querySelector("#txt-edit-title").value = null; //ito ang mag cclear ng inputs sa title after pindutin ang create button. 
		document.querySelector("#txt-edit-title").value = null;
		//ito ang mag cclear ng inputs sa body after pindutin ang create button.

		document.querySelector("#btn-submit-update").setAttribute("disabled", true); //ito ang nag papabalik sa pagiging disabled ng button create natin.
	})
});


/*
	Activity:
		- Write the neccessary code to delete a post.
		- Make a function called deletePost.
		- Pass a parameter id or follow the typicode route for delete.
		- Use fetch method and add the options.
		- Send a screenshot of your final result.
*/
const deletePost = (id)=> {
	let url = `https://jsonplaceholder.typicode.com/posts/${id}`
	fetch(url, {
		method: "DELETE",
		headers: {"Content-type": "application/json; charset=UTF-8"}
	})
	.then((data)=> {
		document.querySelector(`#post-${id}`).remove();
	})
};